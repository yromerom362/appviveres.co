<?php 
  //ob_start(); 
  require_once('includes/load.php');
  //if($session->isUserLoggedIn(true)) { redirect('home.php', false);}



?>

<!DOCTYPE HTML>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>Viveres.co</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/color.css">

        <link rel="shortcut icon" href="images/favicoon.ico">
        <!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=719ece0e-2c03-4167-b516-3416381a0c4e"> </script>
<!-- End of  Zendesk Widget script -->


<script type="text/javascript">
$(document).ready(function() {
    $('#send').click(function(){
        if($("#email").val().indexOf('@', 0) == -1 || $("#correo").val().indexOf('.', 0) == -1) {
            alert('El correo electrónico introducido no es correcto.');
            return false;
        }

        alert('El email introducido es correcto.');
    });
});
</script>


    </head>
    <body>
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <div id="main">
            <!-- header -->
            <header class="main-header">
                <a href="index.php" class="logo-holder"><img src="images/logoo.png" alt=""></a>       
                <div class="header-search_btn show-search-button"><i class="fal fa-search"></i><span>Busqueda</span></div>

                <a href="dashboard-add-listing.html" class="add-list color-bg">Agregar negocio <span><i class="fal fa-layer-plus"></i></span></a>
                
                <div class="cart-btn   show-header-modal" data-microtip-position="bottom" role="tooltip" aria-label="Your Wishlist"><i class="fal fa-heart"></i><span class="cart-counter green-bg"></span> </div>
                >
                <?php
                //echo display_msg($msg); 
                ?>
                <div class="lang-wrap">
                    <div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>Es</strong></span><i class="fa fa-caret-down arrlan"></i></div>
                    <ul class="lang-tooltip lang-action no-list-style">
                        <li><a href="index.php" class="current-lan" data-lantext="Es">Español</a></li>
                        <li><a href="indexen.html" data-lantext="En">English</a></li>
                       <li><a href="indexfr.html" data-lantext="Fr">Français</a></li>
                    </ul>
                </div>
               
                <div class="nav-button-wrap color-bg">
                    <div class="nav-button">
                        <span></span><span></span><span></span>
                    </div>
                </div>
                
                <div class="nav-holder main-menu">
                    <nav>
                        <ul class="no-list-style">
                            <li>
                                <a href="index.php">Home</a>
                                
                               
                            </li>
                            <li>
                                <a href="tiendas.html">Tiendas </a>
                                <!--second level -->
                            </li>
                            <li>
                                <a href="productos.html">Productos</a>
                            </li>
                            <li>

                            
                                <a href="comofunciona.html">Como funciona</a>
                            
                        </ul>
                    </nav>
                </div>
                                    
                <div class="header-search_container header-search vis-search">
                    <div class="container small-container">
                        <div class="header-search-input-wrap fl-wrap">
                            
                            <div class="header-search-input">
                                <label><i class="fal fa-keyboard"></i></label>
                                <input type="text" placeholder="What are you looking for ?"   value=""/>  
                            </div>
                            
                            <div class="header-search-input location autocomplete-container">
                                <label><i class="fal fa-map-marker"></i></label>
                                <input type="text" placeholder="Location..." class="autocomplete-input" id="autocompleteid2" value=""/>
                                <a href="#"><i class="fal fa-dot-circle"></i></a>
                            </div>
                            
                            <div class="header-search-input header-search_selectinpt ">
                                <select data-placeholder="Category" class="chosen-select no-radius" >
                                    <option>All Categories</option>
                                    <option>All Categories</option>
                                    <option>Shops</option>
                                    <option>Hotels</option>
                                    <option>Restaurants</option>
                                    <option>Fitness</option>
                                    <option>Events</option>
                                </select>
                            </div>
                        
                            <button class="header-search-button green-bg" onclick="window.location.href='listing.html'"><i class="far fa-search"></i> Search </button>
                        </div>
                        <div class="header-search_close color-bg"><i class="fal fa-long-arrow-up"></i></div>
                    </div>
                </div>
                
            </header>

            <div id="wrapper">
                <div class="content">
                    
                  
                    <section class="slw-sec" id="sec1">
                        <div class="section-title">
                            <h2>Has parte de nuestra red</h2>
                            <div class="section-subtitle">Has parte de nuestra red</div>
                            <span class="section-separator"></span>
                            <p> Ingresa tus datos para tener acceso.</p>
                        </div>  
                    </section>
                    
                    <section class="gray-bg small-padding no-top-padding-sec" id="sec1">
                        <div class="container">
                            
                            <div class="mob-nav-content-btn  color2-bg show-list-wrap-search ntm fl-wrap"><i class="fal fa-filter"></i>  Registrate</div>
                            <div class="fl-wrap">
                                <div class="row">
                                <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <div class=" fl-wrap lws_mobile   tabs-act block_box">
                                            <div class="filter-sidebar-header fl-wrap" id="filters-column">
                                                <ul class="tabs-menu fl-wrap no-list-style">
                                                    <li class="current"><a href="#filters-search"> <i class="fal fa-sliders-h"></i> Registrate </a></li>
                                                </ul>
                                            </div>
                                            <div class="scrl-content filter-sidebar    fs-viscon">
                                                               
                                                <div class="tabs-container fl-wrap">
                                                  
                                                    <div class="tab">
                                                        
        <form action="includes/reg_user.php" method="POST" name="registrarse">
                <div class="listsearch-input-item">
                    <span class="iconn-dec"><i class="far fa-user"></i></span>
                    <input type="text" placeholder="nombre" name="nombre" value=""/>
                </div>
                <div class="listsearch-input-item">
                    <span class="iconn-dec"><i class="far fa-bookmark"></i></span>
                    <input type="text" placeholder="apellido" name="apellido" value=""/>
                </div>
                <div class="listsearch-input-item">
                    <span class="iconn-dec"><i class="far fa-envelope"></i></span>
                    <input type="enteremail" placeholder="correo" name="correo" value=""/>
                </div>
                <div class="listsearch-input-item">
                    <span class="iconn-dec"><i class="far fa-user"></i></span>
                    <input type="text" placeholder="password" name="password" value=""/>
                </div>
                
                <div class="listsearch-input-item">
                <input class="header-search-button color-bg" name="submit" type="submit" value="Suscribirse" />
                </button>
                </div>  
        </form>
                                                        
                                                    </div>
                                                </div>                    
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2"></div>
                                </div>
                            </div>
                        </div>
                    </section>         
                </div>
            </div>
           
            <footer class="main-footer fl-wrap">
                
                <div class="footer-header fl-wrap grad ient-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5">
                                <div  class="subscribe-header">
                                    <h3>Subscribete <span>para conocerne y actualizarte de las nevas tiendas</span></h3>
                                    <p>Quieres que se te notifique las nuevas tiendas ?  Ingresa.</p>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="subscribe-widget">
                                    <div class="subcribe-form">
                                        <form id="subscribe">
                                            <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="Escribe tu correo" spellcheck="false" type="text">
                                            <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fal fa-envelope"></i></button>
                                            <label for="subscribe-email" class="subscribe-message"></label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-->
               
                <div class="footer-inner   fl-wrap">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <div class="footer-logo"><a href="index.php"><img src="images/logoo.png" alt=""></a></div>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <p> La forma mas rapida, facil y economica de comprar viveres y provisiones en tu tienda cercana!   </p>
                                        <ul  class="footer-contacts fl-wrap no-list-style">
                                            <li><span><i class="fal fa-envelope"></i> Correo :</span><a href="#" target="_blank">contacto@viveres.co</a></li>
                                            <li> <span><i class="fal fa-map-marker"></i> Direccion :</span><a href="#" target="_blank">11a - 94 Calle 95, Chicó Norte, Bogota, Colombia</a></li>
                                            <li><span><i class="fal fa-phone"></i> Telefono :</span><a href="#">+57 3204283116</a></li>
                                        </ul>
                                        <div class="footer-social">
                                            <span>Encuentranos en: </span>
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3>Blog</h3>
                                    <div class="footer-widget-posts fl-wrap">
                                        <ul class="no-list-style">
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="images/photoshop/comida2.png" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Tiendas ya la nueva tienda disponible en nuestra APP</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 21 Mayo 09.05 </span> 
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="images/photoshop/comida1.png" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title=""> Cafecito tienda disponible en nuestra APP</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 7 Mar 18.21 </span> 
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="images/photoshop/natuorganic.png" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Natuorganic nueva tienda disponible en nuestra APP</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 5 Mar 16.42 </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <a href="blog.html" class="footer-link"> Màs noticias <i class="fal fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                       </div>
                
        <script src="js/jquery.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY_HERE&libraries=places&callback=initAutocomplete"></script>
        <script src="js/map-single.js"></script>     
        <script type="text/javascript" src="../jquery.js"></script>
                     
    </body>
</html>