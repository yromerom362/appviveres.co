<?php 
require_once('../includes/config.php');

$request = file_get_contents("php://input");
$fecha = date('Y-m-d H:i:s');

$registro = $fecha." - Ingreso de mensaje - ".$request."\n";

file_put_contents("registro_de_actualizaciones.log", $registro, FILE_APPEND);
$request = json_decode($request);


function telegram($msg) {
        global $telegrambot,$telegramchatid;
        $url='https://api.telegram.org/bot'.$telegrambot.'/sendMessage';$data=array('chat_id'=>$telegramchatid,'text'=>$msg);
        $options=array('http'=>array('method'=>'POST','header'=>"Content-Type:application/x-www-form-urlencoded\r\n",'content'=>http_build_query($data),),);
        $context=stream_context_create($options);
        $result=file_get_contents($url,false,$context);
        return $result;
}
$telegrambot = 'TOKEN_TELEGRAM';
$telegramchatid = $request->message->chat->id;
$mensaje = $request->message->text;
$usuario = $request->message->from->first_name." ".$request->message->from->last_name;


if($mensaje == "/start"){
	$respuesta = "Bienvenido ".$usuario;
	$logSeguimiento = "Mensaje bienvenida - " . $respuesta ;
	file_put_contents("registro_de_actualizaciones.log", $logSeguimiento, FILE_APPEND);
}
else if($mensaje == "/tiendas"){
    
    $sql = "SELECT * FROM `tiendas` order by 1";
    
        $result = $mysqli->query($sql) or die($mysqli->error);
        $row_cnt = $result->num_rows;

        if($row_cnt>0){
            $tiendas="";
            
            while ($fila = $result->fetch_assoc()) {                
                $tiendas .=$fila['Nombre'] . " - ";                
            }

            $respuesta = "Tenemos las siguentes tiendas disponibles: ".$tiendas;
            $logSeguimiento = "Mensaje restaurantes - " . $respuesta ;
	        file_put_contents("registro_de_actualizaciones.log", $logSeguimiento, FILE_APPEND);
          
        }else{
            $respuesta = "No hay tiendas disponibles!";
            $logSeguimiento = "Mensaje tiendas - " . $respuesta ;
	        file_put_contents("registro_de_actualizaciones.log", $logSeguimiento, FILE_APPEND);          
        }
}
else if($mensaje != "/tiendas"){
    $sql = "SELECT * FROM `tiendas` where `nombre` = '$mensaje'";

    $result = $mysqli->query($sql) or die($mysqli->error);
    $row_cnt = $result->num_rows;

    if($row_cnt>0){     
        while ($fila = $result->fetch_assoc()) {      
        $respuesta = "La información de la tienda ". $fila['Nombre']." es, Barrio: ".$fila['Barrio']." - Direccion: ".$fila['Direccion']." - Telefono: ".$fila['Telefono']." - Celular: ".$fila['Celular'];
        }
        $logSeguimiento = "Mensaje restaurantes - " . $respuesta ;
        file_put_contents("registro_de_actualizaciones.log", $logSeguimiento, FILE_APPEND);
      
    }else{
        $respuesta = "Verifique la tienda, no se reconoce ese nombre " ;
        $logSeguimiento = "Mensaje tiendas - " . $respuesta ;
        file_put_contents("registro_de_actualizaciones.log", $logSeguimiento, FILE_APPEND);          
    }
}
else{
	$respuesta = "No se encontró el comando: ".$mensaje;	
	$logSeguimiento = "Mensaje Error - " . $respuesta ;
	file_put_contents("registro_de_actualizaciones.log", $logSeguimiento, FILE_APPEND);
}
telegram ($respuesta);

?>