<?php 
  //ob_start(); 
  session_start();
  require_once('includes/load.php');
  //if($session->isUserLoggedIn(true)) { redirect('home.php', false);}
?>

<!DOCTYPE HTML>
<html lang="es">
    <head>

        <meta charset="UTF-8">
        <title>Viveres.co</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <meta name="robots" content="index, follow"/>
        <meta name="keywords" content=""/>
        <meta name="description" content=""/>
        <link type="text/css" rel="stylesheet" href="css/reset.css">
        <link type="text/css" rel="stylesheet" href="css/plugins.css">
        <link type="text/css" rel="stylesheet" href="css/style.css">
        <link type="text/css" rel="stylesheet" href="css/color.css">

        <link rel="shortcut icon" href="images/favicoon.ico">
        <!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=719ece0e-2c03-4167-b516-3416381a0c4e"> </script>
<!-- End of  Zendesk Widget script -->
    </head>
    <body>
        <div class="loader-wrap">
            <div class="loader-inner">
                <div class="loader-inner-cirle"></div>
            </div>
        </div>
        <div id="main">
            <!-- header -->
            <header class="main-header">
                <a href="index.php" class="logo-holder"><img src="images/logoo.png" alt=""></a>       
                <div class="header-search_btn show-search-button"><i class="fal fa-search"></i><span>Busqueda</span></div>

                <a href="dashboard-add-listing.html" class="add-list color-bg">Agregar negocio <span><i class="fal fa-layer-plus"></i></span></a>
                
                
                <div class="cart-btn   show-header-modal" data-microtip-position="bottom" role="tooltip" aria-label="Your Wishlist">
                    <i class="fal fa-heart"></i><span class="cart-counter green-bg"></span> 
                </div>
    <?php 
        if(isset($_SESSION["nombre"])) {
    ?>
            <div class="show-reg-form avatar-img" data-srcav="images/avatar/3.jpg">
                 <?php 
                  $nombre = $_SESSION["nombre"];
                  $apellido = $_SESSION["apellido"];
                  echo "$nombre $apellido";
                 ?>
                 
            </div>
            <!-- Enlace Ingresar -->
            <div class="show-reg-form avatar-img" data-srcav="images/avatar/3.jpg">
                <a href="includes/logout.php">SALIR</a>                  
            </div>
            <!-- Fin Enlace Ingresar -->
    <?php
        }else{
    ?>
        <!-- Enlace Ingresar -->
        <div class="show-reg-form avatar-img" data-srcav="images/avatar/3.jpg">
            <a href="login.php">Ingresar</a>                  
        </div>
        <!-- Fin Enlace Ingresar -->
        
        <!-- Enlace registrarse -->
            <div class="show-reg-form avatar-img" data-srcav="images/avatar/3.jpg">
                <i class="fal fa-user"></i>
                <a href="registro.php">Registrarse</a>                
            </div>
        <!-- Fin Enlace registrarse -->
    <?php
        }
    ?>
                
   
                <div class="lang-wrap">
                    <div class="show-lang"><span><i class="fal fa-globe-europe"></i><strong>Es</strong></span><i class="fa fa-caret-down arrlan"></i></div>
                    <ul class="lang-tooltip lang-action no-list-style">
                        <li><a href="index.php" class="current-lan" data-lantext="Es">Español</a></li>
                        <li><a href="indexen.html" data-lantext="En">English</a></li>
                       <li><a href="indexfr.html" data-lantext="Fr">Français</a></li>
                    </ul>
                </div>
               
                <div class="nav-button-wrap color-bg">
                    <div class="nav-button">
                        <span></span><span></span><span></span>
                    </div>
                </div>
                
                <div class="nav-holder main-menu">
                    <nav>
                        <ul class="no-list-style">
                            <li>
                                <a href="index.php">Home</a>                            </li>
                            <li>
                                <a href="tiendas.html">Tiendas </a>
                                <!--second level -->
                            </li>
                            <li>
                                <a href="productos.html">Productos</a>
                            </li>
                            <li>
                                <a href="comofunciona.html">Como funciona</a>
                        </ul>
                    </nav>
                </div>
                                    
                <div class="header-search_container header-search vis-search">
                    <div class="container small-container">
                        <div class="header-search-input-wrap fl-wrap">
                            
                            <div class="header-search-input">
                                <label><i class="fal fa-keyboard"></i></label>
                                <input type="text" placeholder="What are you looking for ?"   value=""/>  
                            </div>
                            
                            <div class="header-search-input location autocomplete-container">
                                <label><i class="fal fa-map-marker"></i></label>
                                <input type="text" placeholder="Location..." class="autocomplete-input" id="autocompleteid2" value=""/>
                                <a href="#"><i class="fal fa-dot-circle"></i></a>
                            </div>
                            
                            <div class="header-search-input header-search_selectinpt ">
                                <select data-placeholder="Category" class="chosen-select no-radius" >
                                    <option>All Categories</option>
                                    <option>All Categories</option>
                                    <option>Shops</option>
                                    <option>Hotels</option>
                                    <option>Restaurants</option>
                                    <option>Fitness</option>
                                    <option>Events</option>
                                </select>
                            </div>
                        
                            <button class="header-search-button green-bg" onclick="window.location.href='listing.html'"><i class="far fa-search"></i> Search </button>
                        </div>
                        <div class="header-search_close color-bg"><i class="fal fa-long-arrow-up"></i></div>
                    </div>
                </div>
                
            </header>

            <div id="wrapper">
                <div class="content">
                    <section class="hero-section"   data-scrollax-parent="true">
                        <div class="bg-tabs-wrap">
                            <div class="bg-parallax-wrap" data-scrollax="properties: { translateY: '200px' }">
                                <div class="bg bg_tabs"  data-bg="images/bg/hero/fondoindex.PNG"></div>
                                <div class="overlay op7"></div>
                            </div>
                        </div>
                        <div class="container small-container">
                            <div class="intro-item fl-wrap">
                                <span class="section-separator"></span>
                                <div class="bubbles">
                                    <h1>Encuentra tu tienda o supermercado màs cercano</h1>
                                </div>
                                <h3>Todo lo que necesites tiendas, droguerias, carniceria, fruteria y mucho màs.</h3>
                            </div>
                            <!-- main-search-input-tabs-->
                            <div class="main-search-input-tabs  tabs-act fl-wrap">
                                <ul class="tabs-menu change_bg no-list-style">
                                    <li class="current"><a href="#tab-inpt1" data-bgtab="images/bg/hero/1.jpg"> Places</a></li>
                                    <li><a href="#tab-inpt3" data-bgtab="images/bg/hero/1.jpg"> Restaurants</a></li>
                                </ul>
                                <!--tabs -->                       
                                <div class="tabs-container fl-wrap  ">
                                    <!--tab -->
                                    <div class="tab">
                                        <div id="tab-inpt1" class="tab-content first-tab">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-keyboard"></i></label>
                                                        <input type="text" placeholder="Que estas buscando?" value=""/>
                                                    </div>
                                                    <div class="main-search-input-item location autocomplete-container">
                                                        <label><i class="fal fa-map-marker-check"></i></label>
                                                        <input type="text" placeholder="Ubicacion" class="autocomplete-input" id="autocompleteid" value=""/>
                                                        <a href="#"><i class="fa fa-dot-circle-o"></i></a>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <select data-placeholder="All Categories"  class="chosen-select" >
                                                            <option>Tiendas</option>
                                                            <option>Droguerias</option>
                                                            <option>Carnicerias</option>
                                                            <option>Fruterias</option>
                                                            <option>Restaurantes</option>
                                                        </select>
                                                    </div>
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Buscar <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="tab">
                                        <div id="tab-inpt2" class="tab-content">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-handshake-alt"></i></label>
                                                        <input type="text" placeholder="Event Name or Place" value=""/>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                  
                                                    </div>
                                                    <div class="main-search-input-item clact date-container">
                                                        <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                                        <input type="text" placeholder="Event Date"     name="datepicker-here"   value=""/>
                                                        <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                                    </div>
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="tab">
                                        <div id="tab-inpt3" class="tab-content">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-cheeseburger"></i></label>
                                                        <input type="text" placeholder="Restaurant Name" value=""/>
                                                    </div>
                                                    <div class="main-search-input-item clact date-container">
                                                        <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                                        <input type="text" placeholder="Date and Time"     name="datepicker-here-time"   value=""/>
                                                        <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-user-friends"></i></label>
                                                        <input type="number" placeholder="Persons" value=""/>
                                                    </div>
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="tab">
                                        <div id="tab-inpt4" class="tab-content">
                                            <div class="main-search-input-wrap fl-wrap">
                                                <div class="main-search-input fl-wrap">
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-cheeseburger"></i></label>
                                                        <input type="text" placeholder="Hotel Name" value=""/>
                                                    </div>
                                                    <div class="main-search-input-item">
                                                        <label><i class="fal fa-user-friends"></i></label>
                                                        <input type="number" placeholder="Persons" value=""/>
                                                    </div>
                                                    <div class="main-search-input-item clact date-container3 daterangepicker_big">
                                                        <span class="iconn-dec"><i class="fal fa-calendar"></i></span>
                                                        <input type="text" placeholder="Date In Out"     name="dates"   value=""/>
                                                        <span class="clear-singleinput"><i class="fal fa-times"></i></span>
                                                    </div>
                                                    <button class="main-search-button color2-bg" onclick="window.location.href='listing.html'">Search <i class="far fa-search"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                                                    
                                </div>
                                
                            </div>
                          
                            <div class="hero-categories fl-wrap">
                                <h4 class="hero-categories_title">Busca por categoria :</h4>
                                <ul class="no-list-style">
                                    <li><a href="tiendas.html"><i class="far fa-shopping-bag"></i><span>Tiendas</span></a></li>
                                    <li><a href="tiendas.html"><i class="far fa-shopping-bag"></i><span>Drogueria</span></a></li>
                                    <li><a href="tiendas.html"><i class="far fa-shopping-bag"></i><span>Carniceria</span></a></li>
                                    <li><a href="tiendas.html"><i class="far fa-shopping-bag"></i><span>Fruteria</span></a></li>
                                    <li><a href="listing.html"><i class="far fa-cheeseburger"></i><span>Restaurantes</span></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="header-sec-link">
                            <a href="#sec1" class="custom-scroll-link"><i class="fal fa-angle-double-down"></i></a> 
                        </div>
                    </section>
                  
                    <section class="slw-sec" id="sec1">
                        <div class="section-title">
                            <h2>Conoce nuestras tiendas</h2>
                            <div class="section-subtitle">Conoce nuestras tiendas</div>
                            <span class="section-separator"></span>
                            <p> Busca los comercios mas cercanos a  tu ubicacion.</p>
                        </div>
                        <div class="listing-slider-wrap fl-wrap">
                            <div class="listing-slider fl-wrap">
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                        
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                            
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>Save</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="images/photoshop/Sanazafran.png" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Abierto</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">Sanazafran Comida sana</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i> Calle N, 71# #11 10, Bogotá, Colombia</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">4.1</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                                    <br>
                                                                    <div class="reviews-count">26 reviews</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category red-bg"><i class="fal fa-cheeseburger"></i></div>
                                                                        <span>Restaurante</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="2"></span>
                                                                        <span class="price-name-tooltip">Pricey</span>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                                                                  
                                            </div>
                                        </div>
                                        
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                               
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>Save</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="images/photoshop/Naturganic.png" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_close"><i class="fal fa-lock"></i>Cerrado</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">Naturganic</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i> Cra. 11 #7340, Bogotá, Colombia</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">5.0</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <br>
                                                                    <div class="reviews-count">7 reviews</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category blue-bg"><i class="fal fa-cart-arrow-down"></i></div>
                                                                        <span>Tienda / Restaurante</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="3"></span>
                                                                        <span class="price-name-tooltip">Moderate</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                <!-- listing-item end -->                                                   
                                            </div>
                                        </div>
                                        <!--  swiper-slide end  -->                                                                                                                      
                                        <!--  swiper-slide  -->
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                                <!-- listing-item  -->
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>Save</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="images/photoshop/tiendasya.png" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Abierto</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">Tiendas ya alamos</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i> Cl 72 101a- 30</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">4.2</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="4"></div>
                                                                    <br>
                                                                    <div class="reviews-count">3 reviews</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category  yellow-bg"><i class="fal fa-cart-arrow-down"></i></div>
                                                                        <span>Hotels</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="4"></span>
                                                                        <span class="price-name-tooltip">Ultra Hight</span>
                                                    </article>
                                                </div>
                                                                                                  
                                            </div>
                                        </div>
                                        
                                        <div class="swiper-slide">
                                            <div class="listing-slider-item fl-wrap">
                                               
                                                <div class="listing-item listing_carditem">
                                                    <article class="geodir-category-listing fl-wrap">
                                                        <div class="geodir-category-img">
                                                            <div class="geodir-js-favorite_btn"><i class="fal fa-heart"></i><span>Save</span></div>
                                                            <a href="listing-single.html" class="geodir-category-img-wrap fl-wrap">
                                                            <img src="images/photoshop/almacafe.png" alt=""> 
                                                            </a>
                                                            <div class="geodir_status_date gsd_open"><i class="fal fa-lock-open"></i>Abierto</div>
                                                            <div class="geodir-category-opt">
                                                                <div class="geodir-category-opt_title">
                                                                    <h4><a href="listing-single.html">Almacafe tienda</a></h4>
                                                                    <div class="geodir-category-location"><a href="#"><i class="fas fa-map-marker-alt"></i> a 8-98,, Cl. 72 #82, Bogotá, Colombia</a></div>
                                                                </div>
                                                                <div class="listing-rating-count-wrap">
                                                                    <div class="review-score">5.0</div>
                                                                    <div class="listing-rating card-popup-rainingvis" data-starrating2="5"></div>
                                                                    <br>
                                                                    <div class="reviews-count">2 reviews</div>
                                                                </div>
                                                                <div class="listing_carditem_footer fl-wrap">
                                                                    <a class="listing-item-category-wrap" href="#">
                                                                        <div class="listing-item-category green-bg"><i class="fal fa-cart-arrow-down"></i></div>
                                                                        <span>Tienda</span>
                                                                    </a>
                                                                    <div class="price-level geodir-category_price">
                                                                        <span class="price-level-item" data-pricerating="4"></span>
                                                                        <span class="price-name-tooltip">Ultra Hight</span>
                                                                    </div>
                                                                
                                                            </div>
                                                        </div>
                                                    </article>
                                                </div>
                                                                                                
                                            </div>
                                        </div>
                                                                               
                                    </div>
                                </div>
                                <div class="listing-carousel-button listing-carousel-button-next2"><i class="fas fa-caret-right"></i></div>
                                <div class="listing-carousel-button listing-carousel-button-prev2"><i class="fas fa-caret-left"></i></div>
                            </div>
                            <div class="tc-pagination_wrap">
                                <div class="tc-pagination2"></div>
                            </div>
                        </div>
                    </section>
                    
					<div class="sec-circle fl-wrap"></div>
                 
                    <section   class="gray-bg hidden-section particles-wrapper">
                        <div class="container">
                            <div class="section-title">
                                <h2>Encuentranos en las siguientes ciudades</h2>
                                <div class="section-subtitle">Catalog of Categories</div>
                                <span class="section-separator"></span>
                                <p>Seguimos creciendo y nos encontramos en las siguientes ciudades.</p>
                            </div>
                            <div class="listing-item-grid_container fl-wrap">
                                <div class="row">
                                    
                                    <div class="col-sm-4">
                                        <div class="listing-item-grid">
                                            <div class="bg"  data-bg="images/photoshop/bogota.png"></div>
                                            <div class="d-gr-sec"></div>
                                            <div class="listing-counter color2-bg"><span>30 </span> Negocios</div>
                                            <div class="listing-item-grid_title">
                                                <h3><a href="listing.html">Bogota</a></h3>
                                                <p>Continuamos creciendo</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--  listing-item-grid end  -->
                                    <!--  listing-item-grid  -->
                                    <div class="col-sm-4">
                                        <div class="listing-item-grid">
                                            <div class="bg"  data-bg="images/photoshop/medellin.png"></div>
                                            <div class="d-gr-sec"></div>
                                            <div class="listing-counter color2-bg"><span>12 </span> Negocios</div>
                                            <div class="listing-item-grid_title">
                                                <h3><a href="listing.html">Medellin</a></h3>
                                                <p>Continuamos creciendo</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="listing-item-grid">
                                            <div class="bg"  data-bg="images/photoshop/cali.png"></div>
                                            <div class="d-gr-sec"></div>
                                            <div class="listing-counter color2-bg"><span>11 </span> Negocios</div>
                                            <div class="listing-item-grid_title">
                                                <h3><a href="listing.html">Cali</a></h3>
                                                <p>Continuamos creciendo</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="listing-item-grid">
                                            <div class="bg"  data-bg="images/photoshop/cdmx.png"></div>
                                            <div class="d-gr-sec"></div>
                                            <<div class="listing-counter color2-bg"><span>7 </span> Negocios</div>
                                            <div class="listing-item-grid_title">
                                                <h3><a href="listing.html">CDMX</a></h3>
                                                <p>Continuamos creciendo</p>
                                            </div>
                                        </div>
                                    </div>
                                  
                                    <div class="col-sm-8">
                                        <div class="listing-item-grid">
                                            <div class="bg"  data-bg="images/photoshop/la.png"></div>
                                            <div class="d-gr-sec"></div>
                                            <div class="listing-counter color2-bg"><span>30 </span> Negocios</div>
                                            <div class="listing-item-grid_title">
                                                <h3><a href="listing.html">Los angeles</a></h3>
                                                <p>Continuamos creciendo</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                    <section class="parallax-section" data-scrollax-parent="true">
                        <div class="bg par-elem "  data-bg="images/bg/1.PNG" data-scrollax="properties: { translateY: '30%' }"></div>
                        <div class="overlay op7"></div>
                        <!--container-->
                        <div class="container">
                            <div class="video_section-title fl-wrap">
                                <h4>Nuestro video</h4>
                                <h2>Mira una explicacion de como trabajamos. <br> Conoce nuestros beneficios</h2>
                            </div>
                            <a href="https://vimeo.com/70851162" class="promo-link big_prom   image-popup"><i class="fal fa-play"></i><span> Video Youtube</span></a>
                        </div>
                    </section>
                    
                    <section      data-scrollax-parent="true">
                        <div class="container">
                            <div class="section-title">
                                <h2>Como trabajamos</h2>
                                <div class="section-subtitle">Discover &amp; Connect </div>
                                <span class="section-separator"></span>
                                <p>En tres pasos y menos de 30 minutos tendras tu domicilio.</p>
                            </div>
                            <div class="process-wrap fl-wrap">
                                <ul class="no-list-style">
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">01 </span>
                                            <div class="time-line-icon"><i class="fal fa-map-marker-alt"></i></div>
                                            <h4> Busca lugares cercanos</h4>
                                            <p>Comparte tu ubicacion y la plataofrma encontrara la tenda mas cercana.</p>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">02</span>
                                            <div class="time-line-icon"><i class="fal fa-mail-bulk"></i></div>
                                            <h4> Contactanos por nuestro chat</h4>
                                            <p>Escribele directamente a tu tiendero o al domiciliario .</p>
                                        </div>
                                        <span class="pr-dec"></span>
                                    </li>
                                    <li>
                                        <div class="process-item">
                                            <span class="process-count">03</span>
                                            <div class="time-line-icon"><i class="fal fa-layer-plus"></i></div>
                                            <h4> Agrega los productos al carrito de compras</h4>
                                            <p> Despues de elegir los productos, realiza el pago y listo en menos de 30 minutos llegara a tu casa.</p>
                                        </div>
                                    </li>
                                </ul>
                                <div class="process-end"><i class="fal fa-check"></i></div>
                            </div>
                        </div>
                    </section>
                    
                    <section class="gradient-bg hidden-section" data-scrollax-parent="true">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="colomn-text  pad-top-column-text fl-wrap">
                                        <div class="colomn-text-title">
                                            <h3>Descarga el APP</h3>
                                            <p>Ya puedes encontrar todos nuestros servicios al alcance de tu mano.</p>
                                            <a href="#" class=" down-btn color3-bg"><i class="fab fa-apple"></i>  Apple Store </a>
                                            <a href="#" class=" down-btn color3-bg"><i class="fab fa-android"></i>  Google Play </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="collage-image">
                                        <img src="images/api.png" class="main-collage-image" alt="">                               
                                        <div class="images-collage-title color2-bg icdec"> <img src="images/logoo.png"   alt=""></div>
                                        <div class="images-collage_icon green-bg" style="right:-20px; top:120px;"><i class="fal fa-thumbs-up"></i></div>
                                        <div class="collage-image-min cim_1"><img src="images/photoshop/comida1.png" alt=""></div>
                                        <div class="collage-image-min cim_2"><img src="images/photoshop/comida2.png" alt=""></div>
                                        <div class="collage-image-btn green-bg">Booking now</div>
                                        <div class="collage-image-input">Search <i class="fa fa-search"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="gradient-bg-figure" style="right:-30px;top:10px;"></div>
                        <div class="gradient-bg-figure" style="left:-20px;bottom:30px;"></div>
                        <div class="circle-wrap" style="left:270px;top:120px;" data-scrollax="properties: { translateY: '-200px' }">
                            <div class="circle_bg-bal circle_bg-bal_small"></div>
                        </div>
                        <div class="circle-wrap" style="right:420px;bottom:-70px;" data-scrollax="properties: { translateY: '150px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:420px;top:-70px;" data-scrollax="properties: { translateY: '100px' }">
                            <div class="circle_bg-bal circle_bg-bal_big"></div>
                        </div>
                        <div class="circle-wrap" style="left:40%;bottom:-70px;"  >
                            <div class="circle_bg-bal circle_bg-bal_middle"></div>
                        </div>
                        <div class="circle-wrap" style="right:40%;top:-10px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                        <div class="circle-wrap" style="right:55%;top:90px;"  >
                            <div class="circle_bg-bal circle_bg-bal_versmall" data-scrollax="properties: { translateY: '-350px' }"></div>
                        </div>
                    </section>
                    
                           
                            <div class="tc-pagination"></div>
                        </div>
                        <div class="waveWrapper waveAnimation">
                          <div class="waveWrapperInner bgMiddle">
                            <div class="wave-bg-anim waveMiddle" style="background-image: url('images/wave-top.png')"></div>
                          </div>
                          <div class="waveWrapperInner bgBottom">
                            <div class="wave-bg-anim waveBottom" style="background-image: url('images/wave-top.png')"></div>
                          </div>
                        </div> 						
                  
                    </section>
                    
                </div>
                <
            </div>
           
            <footer class="main-footer fl-wrap">
                
                <div class="footer-header fl-wrap grad ient-dark">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-5">
                                <div  class="subscribe-header">
                                    <h3>Subscribete <span>para conocerne y actualizarte de las nevas tiendas</span></h3>
                                    <p>Quieres que se te notifique las nuevas tiendas ?  Ingresa.</p>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="subscribe-widget">
                                    <div class="subcribe-form">
                                        <form id="subscribe">
                                            <input class="enteremail fl-wrap" name="email" id="subscribe-email" placeholder="Escribe tu correo" spellcheck="false" type="text">
                                            <button type="submit" id="subscribe-button" class="subscribe-button"><i class="fal fa-envelope"></i></button>
                                            <label for="subscribe-email" class="subscribe-message"></label>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer-->
               
                <div class="footer-inner   fl-wrap">
                    <div class="container">
                        <div class="row">
                            
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <div class="footer-logo"><a href="index.php"><img src="images/logoo.png" alt=""></a></div>
                                    <div class="footer-contacts-widget fl-wrap">
                                        <p> La forma mas rapida, facil y economica de comprar viveres y provisiones en tu tienda cercana!   </p>
                                        <ul  class="footer-contacts fl-wrap no-list-style">
                                            <li><span><i class="fal fa-envelope"></i> Correo :</span><a href="#" target="_blank">contacto@viveres.co</a></li>
                                            <li> <span><i class="fal fa-map-marker"></i> Direccion :</span><a href="#" target="_blank">11a - 94 Calle 95, Chicó Norte, Bogota, Colombia</a></li>
                                            <li><span><i class="fal fa-phone"></i> Telefono :</span><a href="#">+57 3204283116</a></li>
                                        </ul>
                                        <div class="footer-social">
                                            <span>Encuentranos en: </span>
                                            <ul class="no-list-style">
                                                <li><a href="#" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-twitter"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-vk"></i></a></li>
                                                <li><a href="#" target="_blank"><i class="fab fa-whatsapp"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-4">
                                <div class="footer-widget fl-wrap">
                                    <h3>Blog</h3>
                                    <div class="footer-widget-posts fl-wrap">
                                        <ul class="no-list-style">
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="images/photoshop/comida2.png" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Tiendas ya la nueva tienda disponible en nuestra APP</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 21 Mayo 09.05 </span> 
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="images/photoshop/comida1.png" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title=""> Cafecito tienda disponible en nuestra APP</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 7 Mar 18.21 </span> 
                                                </div>
                                            </li>
                                            <li class="clearfix">
                                                <a href="#"  class="widget-posts-img"><img src="images/photoshop/natuorganic.png" class="respimg" alt=""></a>
                                                <div class="widget-posts-descr">
                                                    <a href="#" title="">Natuorganic nueva tienda disponible en nuestra APP</a>
                                                    <span class="widget-posts-date"><i class="fal fa-calendar"></i> 5 Mar 16.42 </span>
                                                </div>
                                            </li>
                                        </ul>
                                        <a href="blog.html" class="footer-link"> Màs noticias <i class="fal fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                       </div>
                
        <script src="js/jquery.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/scripts.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY_HERE&libraries=places&callback=initAutocomplete"></script>
        <script src="js/map-single.js"></script>                          
    </body>
</html>